package top.wefor.circularanim;

import ohos.aafwk.ability.Ability;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.Path;
import ohos.agp.render.PixelMapHolder;
import ohos.agp.utils.Color;
import ohos.agp.utils.Point;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.hiviewdfx.HiLog;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.media.image.common.Size;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created on 16/8/5.
 * <p/>
 * 对 ViewAnimationUtils.createCircularReveal() 方法的封装.
 * <p/>
 * GitHub: https://github.com/XunMengWinter
 * <p/>
 * latest edited date: 2018-05-13 13:25
 *
 * @author ice
 */
public class CircularAnim {

    public static final long PERFECT_MILLS = 618;

    private static Long sPerfectMills;
    private static Long sFullActivityPerfectMills;
    private static Integer sColorOrImageRes;

    /**
     * View visible OnAnimatorDeployListener.
     */
    private static OnAnimatorDeployListener sShowAnimatorDeployListener, sHideAnimatorDeployListener;

    /**
     * Activity OnAnimatorDeployListener.
     */
    private static OnAnimatorDeployListener sStartAnimatorDeployListener, sReturnAnimatorDeployListener;


    private static long getPerfectMills() {
        if (sPerfectMills != null) {
            return sPerfectMills;
        } else {
            return PERFECT_MILLS;
        }
    }

    private static long getFullActivityMills() {
        if (sFullActivityPerfectMills != null) {
            return sFullActivityPerfectMills;
        } else {
            return PERFECT_MILLS;
        }
    }

    private static int getColorOrImageRes() {
        if (sColorOrImageRes != null) {
            return sColorOrImageRes;
        } else {
            return Color.WHITE.getValue();
        }
    }


    public interface OnAnimationEndListener {
        void onAnimationEnd();
    }

    public interface OnAnimatorDeployListener {
        void deployAnimator(AnimatorValue animator);
    }

    public static class VisibleBuilder {

        private Component mAnimView, mTriggerView;

        private Point mTriggerPoint;

        private long mDurationMills = getPerfectMills();

        private boolean isShow;

        private OnAnimatorDeployListener mOnAnimatorDeployListener;

        private OnAnimationEndListener mOnAnimationEndListener;

        float scale = 1;

        private String mDrawable;

        public VisibleBuilder(Component animView, boolean isShow) {
            mAnimView = animView;
            this.isShow = isShow;
        }

        public VisibleBuilder drawable(String  drawable) {
            mDrawable = drawable;
            return this;
        }

        /**
         * set the trigger view.
         * if {@link VisibleBuilder#mTriggerPoint} is null,
         * then will set the mTriggerView's center as trigger point.
         */
        public VisibleBuilder triggerView(Component triggerView) {
            mTriggerView = triggerView;
            return this;
        }

        /**
         * set the trigger point.
         */
        public VisibleBuilder triggerPoint(Point triggerPoint) {
            mTriggerPoint = triggerPoint;
            return this;
        }

        public VisibleBuilder duration(long durationMills) {
            mDurationMills = durationMills;
            return this;
        }

        public VisibleBuilder deployAnimator(OnAnimatorDeployListener onAnimatorDeployListener) {
            mOnAnimatorDeployListener = onAnimatorDeployListener;
            return this;
        }

        public void go() {
            go(null);
        }

        //@SuppressLint("NewApi")
        public void go(OnAnimationEndListener onAnimationEndListener) {
            mOnAnimationEndListener = onAnimationEndListener;

            if (mTriggerPoint == null) {
                if (mTriggerView != null) {
                    final int tvCX = (int)mTriggerView.getContentPositionX() + mTriggerView.getWidth() / 2;
                    final int tvCY = (int)mTriggerView.getContentPositionY() + mTriggerView.getHeight() / 2;

                    final int avLX = (int)mAnimView.getContentPositionX();
                    final int avTY = (int)mAnimView.getContentPositionY();

                    int triggerX = Math.max(avLX, tvCX);
                    triggerX = Math.min(triggerX, avLX + mAnimView.getWidth());

                    int triggerY = Math.max(avTY, tvCY);
                    triggerY = Math.min(triggerY, avTY + mAnimView.getHeight());
                    mTriggerPoint = new Point(triggerX - avLX, triggerY - avTY);
                } else {
                    int centerX = mAnimView.getWidth() / 2;
                    int centerY = mAnimView.getHeight() / 2;
                    mTriggerPoint = new Point(centerX, centerY);
                }
            }

            // 计算水波中心点至 @mAnimView 边界的最大距离
            int maxW = Math.max(mTriggerPoint.getPointXToInt(), mAnimView.getWidth() - mTriggerPoint.getPointXToInt());
            int maxH = Math.max(mTriggerPoint.getPointYToInt(), mAnimView.getHeight() - mTriggerPoint.getPointYToInt());
            // 勾股定理 & 进一法
            int maxRadius = (int) Math.sqrt(maxW * maxW + maxH * maxH) + 1;



            Component.DrawTask task = new Component.DrawTask() {
                @Override
                public void onDraw(Component component, Canvas canvas) {
                    Paint mPaint = new Paint();
                    if (mDrawable != null) {
                        mPaint.setColor(new Color(Color.getIntColor(mDrawable)));
                    } else {
                            mPaint.setColor(Color.WHITE);
                    }
                    mPaint.setStyle(Paint.Style.FILL_STYLE);
                    mPaint.setAntiAlias(true);
                    Path path = new Path();
                    path.addCircle(mTriggerPoint.getPointXToInt(), mTriggerPoint.getPointYToInt(), maxRadius * scale, Path.Direction.CLOCK_WISE);
                    canvas.clipPath(path, Canvas.ClipOp.DIFFERENCE);
                    canvas.drawPaint(mPaint);
                }
            } ;
            mAnimView.addDrawTask(task);

            AnimatorValue anim = new AnimatorValue();
            anim.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
                @Override
                public void onUpdate(AnimatorValue animatorValue, float v) {
                    if (isShow) {
                        scale = v;
                    } else {
                        scale = 1 - v;
                    }
                    mAnimView.invalidate();
                }
            });

            mAnimView.setVisibility(Component.VISIBLE);
            anim.setDuration(mDurationMills);

            anim.setStateChangedListener(new Animator.StateChangedListener() {
                @Override
                public void onStart(Animator animator) {

                }

                @Override
                public void onStop(Animator animator) {

                }

                @Override
                public void onCancel(Animator animator) {

                }

                @Override
                public void onEnd(Animator animation) {
                    //super.onAnimationEnd(animation);
                    doOnEnd();
                }

                @Override
                public void onPause(Animator animator) {

                }

                @Override
                public void onResume(Animator animator) {

                }
            });
            if (mOnAnimatorDeployListener == null)
                mOnAnimatorDeployListener = isShow ? sShowAnimatorDeployListener : sHideAnimatorDeployListener;
            if (mOnAnimatorDeployListener != null)
                mOnAnimatorDeployListener.deployAnimator(anim);
            anim.start();
        }

        private void doOnEnd() {
            if (isShow) {
                mAnimView.setVisibility(Component.VISIBLE);
                visibleBuilder = null;
            } else {
                mAnimView.setVisibility(Component.INVISIBLE);
            }

            if (mOnAnimationEndListener != null)
                mOnAnimationEndListener.onAnimationEnd();
        }

    }


    public static class FullActivityBuilder {
        private Ability mAbility;
        private Component mFullView;
        private Point mTriggerPoint;
        private Component mTriggerView;
        private Component mTriggerParentView;
        private Component mTriggerGrandpaView;
        private int mColorOrImageRes = getColorOrImageRes();
        private String mDrawable = "#A292C7";
        private Long mDurationMills = sFullActivityPerfectMills;
        private OnAnimatorDeployListener mStartAnimatorDeployListener;
        private OnAnimatorDeployListener mReturnAnimatorDeployListener;
        private OnAnimationEndListener mOnAnimationEndListener;
        float scale = 0;

        public FullActivityBuilder(Ability ability, Component fullView, Component triggerView) {
            mAbility = ability;
            mFullView = fullView;
            mTriggerView = triggerView;
            int cx, cy;
            mTriggerParentView = (Component) triggerView.getComponentParent();
            mTriggerGrandpaView = (Component) triggerView.getComponentParent().getComponentParent();
            if (mTriggerGrandpaView != null) {
                cx = (int)triggerView.getContentPositionX() + (int)mTriggerParentView.getContentPositionX() + (int)mTriggerGrandpaView.getContentPositionX() + triggerView.getWidth() / 2;
                cy = (int)triggerView.getContentPositionY() + (int)mTriggerParentView.getContentPositionY() + (int)mTriggerGrandpaView.getContentPositionY() + triggerView.getHeight() / 2;
            } else {
                cx = (int) triggerView.getContentPositionX() + (int) mTriggerParentView.getContentPositionX() + triggerView.getWidth() / 2;
                cy = (int) triggerView.getContentPositionY() + (int) mTriggerParentView.getContentPositionY() + triggerView.getHeight() / 2;
            }
            mTriggerPoint = new Point(cx, cy);
        }

        public FullActivityBuilder(Ability ability, Component fullView, Point triggerPoint) {
            mAbility = ability;
            mFullView = fullView;
            mTriggerPoint = triggerPoint;
        }

        /**
         * set the ripple background drawableRes.
         * this will be override by {@link FullActivityBuilder}
         */
        public FullActivityBuilder colorOrImageRes(int colorOrImageRes) {
            mColorOrImageRes = colorOrImageRes;
            return this;
        }

        /**
         * set the ripple background drawable.
         */
        public FullActivityBuilder drawable(String  drawable) {
            mDrawable = drawable;
            return this;
        }

        public FullActivityBuilder duration(long durationMills) {
            mDurationMills = durationMills;
            return this;
        }

        /**
         * set the start animation interceptor
         */
        public FullActivityBuilder deployStartAnimator(OnAnimatorDeployListener onAnimatorDeployListener) {
            mStartAnimatorDeployListener = onAnimatorDeployListener;
            return this;
        }

        /**
         * set the return animation interceptor
         */
        public FullActivityBuilder deployReturnAnimator(OnAnimatorDeployListener onAnimatorDeployListener) {
            mReturnAnimatorDeployListener = onAnimatorDeployListener;
            return this;
        }

        public void go(OnAnimationEndListener onAnimationEndListener) {
            mOnAnimationEndListener = onAnimationEndListener;

            final Component view = new Component(mAbility);
            int w = mFullView.getWidth();
            int h = mFullView.getHeight();
            ((ComponentContainer)mFullView).addComponent(view, w, h);

            // 计算中心点至view边界的最大距离
            int maxW = Math.max(mTriggerPoint.getPointXToInt(), w - mTriggerPoint.getPointXToInt());
            int maxH = Math.max(mTriggerPoint.getPointYToInt(), h - mTriggerPoint.getPointYToInt());
            final int finalRadius = (int) Math.sqrt(maxW * maxW + maxH * maxH) + 1;

            PixelMap bitmap = null;
            PixelMap mPixelMap = null;
            PixelMapHolder mPixelMapHolder = null;
            if (mColorOrImageRes != sColorOrImageRes) {
                bitmap = getPixelMapFromResource(mAbility, mColorOrImageRes);
                PixelMap.InitializationOptions opts = new PixelMap.InitializationOptions();
                opts.size = new Size(w, h);
                mPixelMap = PixelMap.create(bitmap, opts);
                mPixelMapHolder = new PixelMapHolder(mPixelMap);
            }
            PixelMapHolder finalMPixelMapHolder = mPixelMapHolder;

            Component.DrawTask task = new Component.DrawTask() {
                @Override
                public void onDraw(Component component, Canvas canvas) {
                    if (mColorOrImageRes != sColorOrImageRes) {
                        Paint mPaint = new Paint();
                        mPaint.setAntiAlias(true);
                        Path path = new Path();
                        path.addCircle(mTriggerPoint.getPointXToInt(), mTriggerPoint.getPointYToInt(), finalRadius * scale, Path.Direction.CLOCK_WISE);
                        canvas.clipPath(path, Canvas.ClipOp.INTERSECT);
                        canvas.drawPixelMapHolder(finalMPixelMapHolder, 0, 0, mPaint);
                    } else {
                        Paint mPaint = new Paint();
                        mPaint.setAntiAlias(true);
                        mPaint.setStyle(Paint.Style.FILL_STYLE);
                        mPaint.setColor(new Color(Color.getIntColor(mDrawable)));
                        canvas.drawCircle(mTriggerPoint.getPointXToInt(), mTriggerPoint.getPointYToInt(), finalRadius * scale, mPaint);
                    }
                    if (scale == 1) {
                        ((ComponentContainer)mFullView).removeComponent(view);
                        view.release();
                    }
                }
            } ;
            view.addDrawTask(task);

            AnimatorValue anim = new AnimatorValue();
            anim.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
                @Override
                public void onUpdate(AnimatorValue animatorValue, float v) {
                    scale = v;
                    view.invalidate();
                }
            });

            anim.setDuration(mDurationMills);
            anim.setStateChangedListener(new Animator.StateChangedListener() {
                @Override
                public void onStart(Animator animator) {

                }

                @Override
                public void onStop(Animator animator) {

                }

                @Override
                public void onCancel(Animator animator) {

                }

                @Override
                public void onEnd(Animator animator) {
                    doOnEnd();

                    if (mAbility.isTerminating()) {
                        return;
                    }
                }

                @Override
                public void onPause(Animator animator) {

                }

                @Override
                public void onResume(Animator animator) {

                }
            });
            if (mStartAnimatorDeployListener == null)
                mStartAnimatorDeployListener = sStartAnimatorDeployListener;
            if (mStartAnimatorDeployListener != null)
                mStartAnimatorDeployListener.deployAnimator(anim);
            anim.start();
        }

        public static PixelMap getPixelMapFromResource(Context context, int resourceId) {
            InputStream inputStream = null;
            try {
                // 创建图像数据源ImageSource对象
                inputStream = context.getResourceManager().getResource(resourceId);
                ImageSource.SourceOptions srcOpts = new ImageSource.SourceOptions();
                srcOpts.formatHint = "image/jpg";
                ImageSource imageSource = ImageSource.create(inputStream, srcOpts);

                // 设置图片参数
                ImageSource.DecodingOptions decodingOptions = new ImageSource.DecodingOptions();
                return imageSource.createPixelmap(decodingOptions);
            } catch (IOException e) {
                HiLog.info(null, "IOException");
            } catch (NotExistException e) {
                HiLog.info(null, "NotExistException");
            } finally {
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (IOException e) {
                        HiLog.info(null, "inputStream IOException");
                    }
                }
            }
            return null;
        }

        private void doOnEnd() {
            mOnAnimationEndListener.onAnimationEnd();
        }
    }

    /* 上面为实现逻辑，下面为外部调用方法 */


    /* 伸展并显示@animView */
//    public static VisibleBuilder show(Component animView) {
//        return new VisibleBuilder(animView, true);
//    }
    public static VisibleBuilder visibleBuilder;

    /* 伸展并显示@animView */
    public static VisibleBuilder show(Component animView) {
        if (visibleBuilder == null || animView != visibleBuilder.mAnimView) {
            visibleBuilder = new VisibleBuilder(animView,true);
            return visibleBuilder;
        } else {
            visibleBuilder.isShow = true;
            return visibleBuilder;
        }
    }

    /* 收缩并隐藏@animView */
    public static VisibleBuilder hide(Component animView) {
        visibleBuilder = new VisibleBuilder(animView, false);
        return visibleBuilder;
    }


    /* 以@triggerView 为触发点铺满整个@activity */
    public static FullActivityBuilder fullActivity(Ability ability, Component fullView, Component triggerView) {
        return new FullActivityBuilder(ability, fullView, triggerView);
    }

    /**
     * 设置默认时长，设置充满activity的默认颜色或图片资源
     */
    public static void init(long perfectMills, long fullActivityPerfectMills, int colorOrImageRes) {
        sPerfectMills = perfectMills;
        sFullActivityPerfectMills = fullActivityPerfectMills;
        sColorOrImageRes = colorOrImageRes;
    }

    /**
     * 设置默认时长，设置充满activity的默认颜色或图片资源
     */
    public static void initDefaultDeployAnimators(
            OnAnimatorDeployListener showListener
            , OnAnimatorDeployListener hideListener
            , OnAnimatorDeployListener startAtyListener
            , OnAnimatorDeployListener returnAtyListener
    ) {
        sShowAnimatorDeployListener = showListener;
        sHideAnimatorDeployListener = hideListener;
        sStartAnimatorDeployListener = startAtyListener;
        sReturnAnimatorDeployListener = returnAtyListener;
    }

}
