#Demo
![](https://images.gitee.com/uploads/images/2021/0713/164549_c18a0803_8230582.gif "001.gif")

# CircularAnim

一个简单的圆形揭露动画效果库，可以用作点击显示效果或者页面跳转间的转场动画。
当您显示或隐藏一组 UI 元素时，此动画效果可为用户提供视觉的连续性。

# 缺陷与优点

缺陷：openharmony当前无法通过背景element获取到color，故在show/hide模式中新增drawable()方法设置显示动画view的背景色，默认为白色。
优点：使用起来非常简单，已将动画封装成CircularAnim，只需要一行代码，就可以实现点击后的收缩扩散动画效果。

# Compile

你可以如下compile该项目，也可以直接把这个类CircularAnim拷贝到项目里去。
项目级build.gradle添加：

```
grooxy
allprojects {
		repositories {
			...
			maven { url 'https://jitpack.io' }
		}
	}
```

将下面这行依赖添加到 应用 级build.gradle，随后Sync即可完成

```
implementation 'com.gitee.baijuncheng-open-source:CircularAnim:1.0.0'
```

# 使用方法

为了使用起来简单，我将动画封装成CircularAnim.

现在，让按钮收缩只需一行代码，如下：
`CircularAnim.hide(mChangeBtn).go();`


同理，让按钮伸展开：
`CircularAnim.show(mChangeBtn).go();`


以View为水波触发点收缩其它View:
`CircularAnim.hide(mContentLayout).triggerView(mLogoBtnIv).go();`


以View为水波触发点伸展其它View:
`CircularAnim.show(mContentLayout).triggerView(mLogoBtnIv).go();`


因为openharmony无法通过背景element获取到color，故在show/hide模式中新增drawable()方法设置显示动画view的背景色，默认为白色:
`CircularAnim.show(mChangeBtn2).drawable("#DDFFAA").go();`

水波般铺满指定颜色并启动一个Ability: 

```
CircularAnim.fullActivity(MainActivity.this, mMainLayout, mProgressBar2)
        .go(new CircularAnim.OnAnimationEndListener() {
            @Override
            public void onAnimationEnd() {
                Intent secondIntent = new Intent();
                Operation operation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName("top.wefor.circularanimdemo")
                        .withAbilityName("top.wefor.circularanimdemo.ListActivity")
                        .build();
                secondIntent.setOperation(operation);
                startAbility(secondIntent);
                MainActivity.this.onStop();
            }
        });
```



这里，你还可以放图片：
`.colorOrImageRes(R.mipmap.img_huoer_black)`

同时，你还可以设置时长、半径、转场动画、动画结束监听器等参数。

用起来非常的方便，一切逻辑性的东西都由帮助类搞定。


# Copyright (C) 2017 Trumeet


Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.