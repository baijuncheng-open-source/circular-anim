package top.wefor.circularanimdemo.fragment;

import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.ability.fraction.FractionAbility;
import ohos.aafwk.ability.fraction.FractionScheduler;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.StackLayout;
import top.wefor.circularanim.CircularAnim;
import top.wefor.circularanimdemo.ResourceTable;

/**
 * Created on 2018/7/15.
 *
 * @author ice
 * <p>
 * GitHub: https://github.com/XunMengWinter
 */
public class FragmentTestActivity extends FractionAbility {

    StackLayout mFragmentLayout;
    Component mFullView;
    Button mReplaceFragment;

    Test1Fragment mTest1Fragment;
    Test2Fragment mTest2Fragment;

    private boolean mIsTest2Fragment = false;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_activity_fragment_test);

        mFragmentLayout = (StackLayout)findComponentById(ResourceTable.Id_fragment_layout);
        mFullView = findComponentById(ResourceTable.Id_full_view);

        mReplaceFragment = (Button) findComponentById(ResourceTable.Id_replaceFragment);

        mTest1Fragment = new Test1Fragment();
        mTest2Fragment = new Test2Fragment();

        showFragment(mTest1Fragment);

        mReplaceFragment.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component view) {
                view.setEnabled(false);

                /* show Fragment with CircularAnim */
                CircularAnim.fullActivity(FragmentTestActivity.this, mFragmentLayout, view)
                        .go(new CircularAnim.OnAnimationEndListener() {
                            @Override
                            public void onAnimationEnd() {
                                mFullView.createAnimatorProperty().alpha(0);

                                showFragment(mIsTest2Fragment ? mTest1Fragment : mTest2Fragment);
                                mIsTest2Fragment = !mIsTest2Fragment;
                                view.setEnabled(true);
                            }
                        });

            }
        });
    }

    private void showFragment(Fraction fragment) {
        FractionScheduler transaction = getFractionManager().startFractionScheduler();
        transaction.replace(ResourceTable.Id_fragment_container, fragment);
        transaction.submit();
    }
}
