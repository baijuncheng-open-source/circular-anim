package top.wefor.circularanimdemo.fragment;

import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import top.wefor.circularanimdemo.ResourceTable;

/**
 * Created on 2018/7/15.
 *
 * @author ice
 */
public class Test1Fragment extends Fraction {

    @Override
    public Component onComponentAttached(LayoutScatter inflater, ComponentContainer container, Intent intent) {
        Component view = inflater.parse(ResourceTable.Layout_fragment_test1, container, false);
        return view;
    }
}
