package top.wefor.circularanimdemo;

import ohos.aafwk.ability.AbilityPackage;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import top.wefor.circularanim.CircularAnim;

/**
 * Created on 16/9/24.
 *
 * @author ice
 */
public class App extends AbilityPackage {

    @Override
    public void onInitialize () {
        super.onInitialize();
        // optional. change the default duration and fullActivity resource.
        CircularAnim.init(700, 618, ResourceTable.Color_colorPrimary);
        // optional. set the default duration OnAnimatorDeployListener.
        CircularAnim.initDefaultDeployAnimators(
                new CircularAnim.OnAnimatorDeployListener() {
                    @Override
                    public void deployAnimator(AnimatorValue animator) {
                        animator.setCurveType(Animator.CurveType.ACCELERATE);
                    }
                },
                new CircularAnim.OnAnimatorDeployListener() {
                    @Override
                    public void deployAnimator(AnimatorValue animator) {
                        animator.setCurveType(Animator.CurveType.DECELERATE);
                    }
                },
                null,
                null);
    }
}
