package top.wefor.circularanimdemo;

import ohos.aafwk.ability.fraction.FractionAbility;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;

import java.util.ArrayList;

/**
 * Created on 16/7/20.
 *
 * @author ice
 */
public class ListActivity extends FractionAbility {
    ListContainer mListContainer;
    StackLayout mListLayout;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_activity_list);
        initList();
    }

    private void initList() {
        mListLayout = (StackLayout)findComponentById(ResourceTable.Id_list_layout);
        mListContainer = (ListContainer) findComponentById(ResourceTable.Id_recyclerView);
        ItemProvider itemProvider = new ItemProvider(getData(), this);
        mListContainer.setItemProvider(itemProvider);
    }

    private ArrayList<Item> getData() {
        ArrayList<Item> list = new ArrayList<>();
        int size = 3;
        for (int i = 0; i < size ; i++) {
            list.add(new Item("nice to meet u", "Click Expand"));
        }
        return list;
    }
}
