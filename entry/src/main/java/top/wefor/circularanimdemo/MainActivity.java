package top.wefor.circularanimdemo;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.*;
import ohos.agp.utils.Point;
import top.wefor.circularanim.CircularAnim;

public class MainActivity extends Ability {

    ProgressBar mProgressBar, mProgressBar2;
    Button mChangeBtn, mChangeBtn2, mActivityImageBtn, mActivityColorBtn, mFullWithDrawable, mFragmentDemo;
    Image mLogoBtnIv;
    StackLayout mMainLayout;
    DirectionalLayout mContentLayout;
    boolean isContentVisible = true;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_activity_main);

        mMainLayout = (StackLayout)findComponentById(ResourceTable.Id_main_layout);
        mProgressBar = (RoundProgressBar) findComponentById(ResourceTable.Id_progressBar);
        mChangeBtn = (Button) findComponentById(ResourceTable.Id_change_btn);
        mProgressBar2 = (RoundProgressBar) findComponentById(ResourceTable.Id_progressBar2);
        mChangeBtn2 = (Button) findComponentById(ResourceTable.Id_change_btn2);
        mActivityImageBtn = (Button) findComponentById(ResourceTable.Id_activity_image_btn);
        mActivityColorBtn = (Button) findComponentById(ResourceTable.Id_activity_color_btn);
        mFullWithDrawable = (Button) findComponentById(ResourceTable.Id_fullWithDrawable);
        mLogoBtnIv = (Image) findComponentById(ResourceTable.Id_logoBtn_iv);
        mContentLayout = (DirectionalLayout) findComponentById(ResourceTable.Id_content_layout);
        mFragmentDemo = (Button) findComponentById(ResourceTable.Id_fragmentDemo);

        mChangeBtn.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                mProgressBar.setVisibility(Component.VISIBLE);
                mProgressBar.setMinValue(0);
                mProgressBar.setMaxValue(100);
                AnimatorValue mAnimator = new AnimatorValue();
                mAnimator.setLoopedCount(-1);
                mAnimator.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
                    @Override
                    public void onUpdate(AnimatorValue animatorValue, float v) {
                        mProgressBar.setProgressValue((int)(100*v));
                    }
                });
                mAnimator.start();
                // 收缩按钮
                CircularAnim.hide(mChangeBtn).drawable("#DDFFAA").go();
            }
        });

        mProgressBar.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                mProgressBar.setVisibility(Component.HIDE);
                // 伸展按钮
                CircularAnim.show(mChangeBtn).drawable("#DDFFAA").go();
            }
        });

        mChangeBtn2.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                mProgressBar2.setVisibility(Component.VISIBLE);
                mProgressBar2.setMinValue(0);
                mProgressBar2.setMaxValue(100);
                AnimatorValue mAnimator2 = new AnimatorValue();
                mAnimator2.setLoopedCount(5);
                mAnimator2.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
                    @Override
                    public void onUpdate(AnimatorValue animatorValue, float v2) {
                        mProgressBar2.setProgressValue((int)(100*v2));
                    }
                });
                mAnimator2.setStateChangedListener(new Animator.StateChangedListener() {
                    @Override
                    public void onStart(Animator animator) {

                    }

                    @Override
                    public void onStop(Animator animator) {

                    }

                    @Override
                    public void onCancel(Animator animator) {

                    }

                    @Override
                    public void onEnd(Animator animator) {
                        CircularAnim.fullActivity(MainActivity.this, mMainLayout, mProgressBar2)
                                .go(new CircularAnim.OnAnimationEndListener() {
                                    @Override
                                    public void onAnimationEnd() {
                                        Intent secondIntent = new Intent();
                                        Operation operation = new Intent.OperationBuilder()
                                                .withDeviceId("")
                                                .withBundleName("top.wefor.circularanimdemo")
                                                .withAbilityName("top.wefor.circularanimdemo.ListActivity")
                                                .build();
                                        secondIntent.setOperation(operation);
                                        startAbility(secondIntent);
                                        MainActivity.this.onStop();
                                    }
                                });
                    }

                    @Override
                    public void onPause(Animator animator) {

                    }

                    @Override
                    public void onResume(Animator animator) {

                    }
                });
                mAnimator2.start();
                CircularAnim.hide(mChangeBtn2)
                        .drawable("#DDFFAA")
//                        .endRadius(mProgressBar2.getHeight() / 2)
                        .go();
            }
        });

        mProgressBar2.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                mProgressBar2.setVisibility(Component.HIDE);
                // 伸展按钮
                CircularAnim.show(mChangeBtn2).drawable("#DDFFAA").go();
            }
        });

        mActivityImageBtn.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                // 先将图片展出铺满，然后启动新的Activity
                CircularAnim.fullActivity(MainActivity.this, mMainLayout, component)
                        .colorOrImageRes(ResourceTable.Media_img_huoer_black)
                        .go(new CircularAnim.OnAnimationEndListener() {
                            @Override
                            public void onAnimationEnd() {
                                Intent secondIntent = new Intent();
                                Operation operation = new Intent.OperationBuilder()
                                        .withDeviceId("")
                                        .withBundleName("top.wefor.circularanimdemo")
                                        .withAbilityName("top.wefor.circularanimdemo.ListActivity")
                                        .build();
                                secondIntent.setOperation(operation);
                                startAbility(secondIntent);
                            }
                        });
            }
        });

        mActivityColorBtn.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                // 先将颜色展出铺满，然后启动新的Activity
                CircularAnim.fullActivity(MainActivity.this, mMainLayout, component)
//                        .colorOrImageRes(R.color.colorPrimary)  //注释掉，因为该颜色已经在App.class 里配置为默认色
                        .deployReturnAnimator(new CircularAnim.OnAnimatorDeployListener() {
                            @Override
                            public void deployAnimator(AnimatorValue animator) {
                                //this .setDuration() with override CircularAnim.setDuration().
                                animator.setDuration(700L);
                                animator.setCurveType(Animator.CurveType.ACCELERATE);
                            }
                        })
                        .go(new CircularAnim.OnAnimationEndListener() {
                            @Override
                            public void onAnimationEnd() {
                                Intent secondIntent = new Intent();
                                Operation operation = new Intent.OperationBuilder()
                                        .withDeviceId("")
                                        .withBundleName("top.wefor.circularanimdemo")
                                        .withAbilityName("top.wefor.circularanimdemo.ListActivity")
                                        .build();
                                secondIntent.setOperation(operation);
                                startAbility(secondIntent);
                            }
                        });
            }
        });

        mFullWithDrawable.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
//                ShapeElement shapeElement = new ShapeElement();
//                shapeElement.setRgbColor(new RgbColor(153, 99, 131));
                CircularAnim.fullActivity(MainActivity.this, mMainLayout, component)
                        .drawable("#996383") // drawable will override colorOrImageRes
                        .go(new CircularAnim.OnAnimationEndListener() {
                            @Override
                            public void onAnimationEnd() {
                                Intent secondIntent = new Intent();
                                Operation operation = new Intent.OperationBuilder()
                                        .withDeviceId("")
                                        .withBundleName("top.wefor.circularanimdemo")
                                        .withAbilityName("top.wefor.circularanimdemo.ListActivity")
                                        .build();
                                secondIntent.setOperation(operation);
                                startAbility(secondIntent);
                            }
                        });
            }
        });

        mLogoBtnIv.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (isContentVisible) {
                    CircularAnim.hide(mContentLayout).duration(5_000).triggerView(mLogoBtnIv).go();
                } else {
                    CircularAnim.show(mContentLayout)
                            .triggerPoint(new Point(mContentLayout.getWidth(), 0))
                            .duration(5_000)
                            .go();
                }
                isContentVisible = !isContentVisible;
            }
        });

        mFragmentDemo.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Intent secondIntent = new Intent();
                Operation operation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName("top.wefor.circularanimdemo")
                        .withAbilityName("top.wefor.circularanimdemo.fragment.FragmentTestActivity")
                        .build();
                secondIntent.setOperation(operation);
                startAbility(secondIntent);

            }
        });
    }

}
