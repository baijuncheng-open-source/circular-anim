/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.wefor.circularanimdemo;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.*;
import top.wefor.circularanim.CircularAnim;

import java.util.List;

public class ItemProvider extends BaseItemProvider {
    private List<Item> list;
    private Ability ability;
    public ItemProvider(List<Item> list, Ability ability) {
        this.list = list;
        this.ability = ability;
    }
    @Override
    public int getCount() {
        return list == null ? 0 : list.size();
    }
    @Override
    public Object getItem(int position) {
        if (list != null && position >= 0 && position < list.size()){
            return list.get(position);
        }
        return null;
    }
    @Override
    public long getItemId(int position) {
        return position;
    }
    @Override
    public Component getComponent(int position, Component convertComponent, ComponentContainer componentContainer) {
        final Component cpt;
        if (convertComponent == null) {
            cpt = LayoutScatter.getInstance(ability).parse(ResourceTable.Layout_item_list, null, false);
        } else {
            cpt = convertComponent;
        }
        Item item = list.get(position);
        Text text = (Text)cpt.findComponentById(ResourceTable.Id_item_index);
        Button button = (Button) cpt.findComponentById(ResourceTable.Id_button);
        button.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                CircularAnim.fullActivity(ability, ability.findComponentById(ResourceTable.Id_list_layout), component)
//                        .colorOrImageRes(R.color.colorPrimary)  //注释掉，因为该颜色已经在App.class 里配置为默认色
                        .go(new CircularAnim.OnAnimationEndListener() {
                            @Override
                            public void onAnimationEnd() {
                                Intent secondIntent = new Intent();
                                Operation operation = new Intent.OperationBuilder()
                                        .withDeviceId("")
                                        .withBundleName("top.wefor.circularanimdemo")
                                        .withAbilityName("top.wefor.circularanimdemo.MainActivity")
                                        .build();
                                secondIntent.setOperation(operation);
                                ability.startAbility(secondIntent);
                            }
                        });
            }
        });
        text.setText(item.getName());
        button.setText(item.getButtontext());
        return cpt;
    }
}
